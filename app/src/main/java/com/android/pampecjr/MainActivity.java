package com.android.pampecjr;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        webView = (WebView) findViewById(R.id.webview);

        final String pampec_site = "http://www.pampecjr.com";
        final AppCompatActivity activity = this;

        mProgressBar.setProgress(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(pampec_site);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("linkedin")) {
                    Intent new_page = MainActivity.newLinkedInIntent(activity.getPackageManager(),url);
                    activity.startActivity(new_page);
                } else if (url.contains("instagram")) {
                    Intent new_page = MainActivity.newInstagramIntent(activity.getPackageManager(),url);
                    activity.startActivity(new_page);
                } else if (url.contains("facebook")) {
                    Intent new_page = MainActivity.newFacebookIntent(activity.getPackageManager(),url);
                    activity.startActivity(new_page);
                } else if (url.contains("pampecjr")) {
                    view.loadUrl(url);
                } else {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(url)));
                }
                return true;
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                mProgressBar.setProgress(progress);
                if (progress == 100) {
                    mProgressBar.setProgress(0);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        mProgressBar.setProgress(0);
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }

        return super.onKeyDown(keyCode,event);
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static Intent newInstagramIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.instagram.android", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse(url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static Intent newLinkedInIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.linkedin.android", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse(url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }
}
